import React, { useEffect } from "react";
import styles from "./HomePage.module.css";
import {
  Footer,
  Header,
  SideMenu,
  Carousel,
  ProductCollection,
  Partner,
} from "@/components";
import { Col, Row, Typography } from "antd";
import { productList1, productList2, productList3 } from "./mockups";
import sideImg1 from "@/assets/images/sider_2019_12-09.png";
import sideImg2 from "@/assets/images/sider_2019_02-04.png";
import sideImg3 from "@/assets/images/sider_2019_02-04-2.png";
import { BaseType } from "antd/lib/typography/Base";

const productList: {
  img: string;
  items: any[];
  title: string;
  type: BaseType; // 因为是定义好了的type类型 所以不用写完整的类型
}[] = [
  {
    img: sideImg1,
    items: productList1,
    title: "爆款推荐",
    type: "warning",
  },
  {
    img: sideImg2,
    items: productList2,
    title: "新品上市",
    type: "danger",
  },
  {
    img: sideImg3,
    items: productList3,
    title: "国内旅游推荐",
    type: "success",
  },
];

export const HomePage: React.FC = (props) => {
  useEffect(() => {
    console.log("kkk");
    console.log("HomePage props", props);
  }, []);
  return (
    <div className={styles.App}>
      <Header />
      <div className={styles.page_content}>
        <Row style={{ marginTop: 20 }}>
          <Col span={6}>
            <SideMenu />
          </Col>
          <Col span={18}>
            <Carousel />
          </Col>
        </Row>
        {productList.map(({ title, img, items, type },index) => (
          <ProductCollection
            title={
              <Typography.Title level={3} type={type}>
                {title}
              </Typography.Title>
            }
            sideImage={img}
            products={items}
            key={index}
          />
        ))}
        <Partner />
      </div>
      <Footer />
    </div>
  );
};
