import { ProductComments } from "@/components";
import { ProductIntro } from "@/components/productIntro";
import { Col, Row, Spin, DatePicker, Anchor, Menu, Divider, Typography, Button } from "antd";
import { ShoppingCartOutlined } from "@ant-design/icons";
// import axios from "axios";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import styles from "./DetailPage.module.css";
import { commentMockData } from "./mockup";
import { useSelector, useDispatch } from "../../redux/hooks";
// import { useDispatch } from "react-redux";
// import { productDetailSlice } from "@/redux/productDetail/slice";
import { getProductDetail } from "@/redux/productDetail/slice";
import { addShoppingCartItem } from "@/redux/shoppingCart/slice";

import { MainLayout } from "@/layouts";

const { RangePicker } = DatePicker;

type MatchParams = {
  touristRouteId: string;
};

export const DetailPage: React.FC = () => {
  // const {touristRouteId} = useParams();
  const { touristRouteId } = useParams<MatchParams>();
  // const [loading, setLoading] = useState<boolean>(true);
  // const [product, setProduct] = useState<any>(null);
  // const [error, setError] = useState<string | null>(null);
  // ! 9.6 使用redux-toolkit 并且使用useSelector实现rudex
  const loading = useSelector((state) => state.productDetail.loading);
  const error = useSelector((state) => state.productDetail.error);
  const product = useSelector((state) => state.productDetail.data);

  // 加入购物车的实现
  const jwt = useSelector((state) => state.user.token);
  const shoppingCartLoading = useSelector((s) => s.shoppingCart.loading);

  // ! 9.6 使用dispatch钩子
  const dispatch = useDispatch();

  // 获取参数
  console.log("touristRouteId", touristRouteId);
  useEffect(() => {
    // const fetchData = async () => {
    //   // setLoading(true);
    //   // 用useState改成redux
    //   dispatch(productDetailSlice.actions.fetchStart())
    //   try {
    //     const { data } = await axios.get(
    //       `http://123.56.149.216:8080/api/touristRoutes/${touristRouteId}`
    //     );
    //     dispatch(productDetailSlice.actions.fetchSuccess(data))
    //     // setProduct(data);
    //     // setLoading(false);
    //   } catch (error) {
    //     // setError(error.message);
    //     // setLoading(false);
    //     dispatch(productDetailSlice.actions.fetchFail(error.message))
    //   }
    // };
    // fetchData();
    // ! 9.8 使用thunk异步方法
    if (touristRouteId) {
      dispatch(getProductDetail(touristRouteId));
    }
  }, []);

  if (loading) {
    return (
      <Spin
        size="large"
        style={{
          marginTop: 200,
          marginBottom: 200,
          marginLeft: "auto",
          marginRight: "auto",
          width: "100%",
        }}
      />
    );
  }
  if (error) {
    return <div>网站出错：{error}</div>;
  }

  return (
    <MainLayout>
      <div className={styles["page-content"]}>
        {/* 产品简介 与 日期选择 */}
        <div className={styles["product-intro-container"]}>
          <Row>
            <Col span={13}>
              <ProductIntro
                title={product.title}
                shortDescription={product.description}
                price={product.originalPrice}
                coupons={product.coupons}
                points={product.points}
                discount={product.price}
                rating={product.rating}
                pictures={product.touristRoutePictures.map((p) => p.url)}
              />
            </Col>
            <Col span={11}>
              <Button
                style={{ marginTop: 50, marginBottom: 30, display: "block" }}
                onClick={() => {
                  dispatch(addShoppingCartItem({ jwt, touristRouteId: product.id }));
                }}
                danger
                type="primary"
                loading={shoppingCartLoading}
              >
                <ShoppingCartOutlined />
                加入购物车
              </Button>
              <RangePicker open style={{ marginTop: 20 }} />
            </Col>
          </Row>
        </div>
        {/* 锚点菜单 */}
        <Anchor className={styles["product-detail-anchor"]}>
          <Menu mode="horizontal">
            <Menu.Item key="1">
              <Anchor.Link href="#feature" title="产品特色"></Anchor.Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Anchor.Link href="#fees" title="费用"></Anchor.Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Anchor.Link href="#notes" title="预订须知"></Anchor.Link>
            </Menu.Item>
            <Menu.Item key="5">
              <Anchor.Link href="#comments" title="用户评价"></Anchor.Link>
            </Menu.Item>
          </Menu>
        </Anchor>
        {/* 产品特色 */}
        <div id="feature" className={styles["product-detail-container"]}>
          <Divider orientation={"center"}>
            <Typography.Title level={3}>产品特色</Typography.Title>
          </Divider>
          <div dangerouslySetInnerHTML={{ __html: product.features }} style={{ margin: 50 }}></div>
        </div>
        <div id="fees" className={styles["product-detail-container"]}>
          <Divider orientation={"center"}>
            <Typography.Title level={3}>费用</Typography.Title>
          </Divider>
          <div dangerouslySetInnerHTML={{ __html: product.fees }} style={{ margin: 50 }}></div>
        </div>
        {/* 预订须知 */}
        <div id="notes" className={styles["product-detail-container"]}>
          <Divider orientation={"center"}>
            <Typography.Title level={3}>预定须知</Typography.Title>
          </Divider>
          <div dangerouslySetInnerHTML={{ __html: product.notes }} style={{ margin: 50 }}></div>
        </div>
        {/* 商品评价*/}
        <div id="comments" className={styles["product-detail-container"]}>
          <Divider orientation={"center"}>
            <Typography.Title level={3}>用户评价</Typography.Title>
          </Divider>
          <div style={{ margin: 40 }}>
            <ProductComments data={commentMockData} />
          </div>
        </div>
      </div>
    </MainLayout>
  );
};
