import React from "react";
import { Row, Col } from "antd";
import { PaymentForm, CheckOutCard } from "@/components";
import { useSelector, useDispatch } from "@/redux/hooks";
import { placeOrder } from "@/redux/order/slice";
import { MainLayout } from "../../layouts/mainLayout";

export const PlaceOrderPage: React.FC = () => {
  const jwt = useSelector((s) => s.user.token);
  const currentOrder = useSelector((s) => s.order.currentOrder);
  const orderLoading = useSelector((s) => s.order.loading);

  const dispatch = useDispatch();
  return (
    <MainLayout>
      <Row>
        <Col span={12}>
          <PaymentForm />
        </Col>
        <Col span={12}>
          <CheckOutCard
            loading={orderLoading}
            order={currentOrder}
            onCheckout={() => {
              dispatch(placeOrder({ jwt, orderId: currentOrder.id }));
            }}
          />
        </Col>
      </Row>
    </MainLayout>
  );
};
