// ? 中间件 ==>柯里化函数
import { Middleware } from "redux";

export const actionLog: Middleware = (store) => (next) => (action) => {
  console.log("state 更新前", store.getState());
  console.log("action", action);
  next(action); // 执行action
  console.log("state 更新后", store.getState());
};

// 执行的时候是
// actionLog(store)(next)(action)
