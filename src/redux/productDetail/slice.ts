// 使用rudux-toolkit来实现reducer
// 2. 引入createSlice 返回reducer
import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

// 1.定义state
interface ProductDetailState {
  data: any;
  loading: boolean; // 请求状态
  error: string | null; // 错误信息
}

const defaultState: ProductDetailState = {
  data: null,
  loading: true,
  error: null,
};

// ! 使用 createAsyncThunk api来实现异步的action 但是要使用redux-toolkit的configureStore
/**
 * @params1 命名空间
 * @params2 异步函数
 */
export const getProductDetail = createAsyncThunk(
  "productDetail/getProductDetail",
  async (touristRouteId: string, ) => {
    // thunkAPI
    // thunkAPI.dispatch(productDetailSlice.actions.fetchStart());
    // try {
    //   const { data } = await axios.get(
    //     `http://123.56.149.216:8080/api/touristRoutes/${touristRouteId}`
    //   );
    //   thunkAPI.dispatch(productDetailSlice.actions.fetchSuccess(data));
    // } catch (error) {
    //   thunkAPI.dispatch(productDetailSlice.actions.fetchFail(error.message));
    // }

    // ! 使用createAsyncThunk的返回值 promise三种状态
    const { data } = await axios.get(
      `http://123.56.149.216:8080/api/touristRoutes/${touristRouteId}`
    );
    return data;
  }
);

export const productDetailSlice = createSlice({
  name: "productDetail", // 命名空间
  initialState: defaultState, //初始化数据
  reducers: {
    fetchStart: (state) => {
      // return { ...state, loading: true };// !return 这是react的写法 state是immutable不可以修改的
      state.loading = true; // immer框架 内部的produce实现返回了一个新的object
    },
    fetchSuccess: (state, action) => {
      state.loading = false;
      state.data = action.payload; // 直接用action返回的数据来更新状态
      state.error = null;
    },
    fetchFail: (state, action: PayloadAction<string | null>) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
  extraReducers:{
    // ! 使用createAsyncThunk的返回值 promise三种状态 必须使用extraReducers
    // 使用的是类型
    [getProductDetail.pending.type]: (state) => {
      // return { ...state, loading: true };// !return 这是react的写法 state是immutable不可以修改的
      state.loading = true; // immer框架 内部的produce实现返回了一个新的object
    },
    [getProductDetail.fulfilled.type]: (state, action) => {
      state.loading = false;
      state.data = action.payload; // 直接用payload传参来更新状态
      state.error = null;
    },
    [getProductDetail.rejected.type]: (state, action: PayloadAction<string | null>) => {
      state.loading = false;
      state.error = action.payload;
    },
  }
});
