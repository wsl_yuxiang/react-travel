import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { checkout } from "../shoppingCart/slice";

interface OrderState {
  currentOrder: any;
  loading: boolean; // 请求状态
  error: string | null; // 错误信息
}

const defaultState: OrderState = {
  currentOrder: null,
  loading: false,
  error: null,
};

export const placeOrder = createAsyncThunk(
  "order/placeOrder",
  async (params: { jwt: string; orderId: string }) => {
    // ! 使用createAsyncThunk的返回值 promise三种状态
    const { data } = await axios.post(
      `http://123.56.149.216:8080/api/orders/${params.orderId}/placeOrder`,
      null,
      {
        headers: {
          Authorization: `bearer ${params.jwt}`,
        },
      }
    );
    return data;
  }
);

export const orderSlice = createSlice({
  name: "order",
  initialState: defaultState,
  reducers: {},
  extraReducers: {
    [placeOrder.pending.type]: (state) => {
      state.loading = true;
    },
    [placeOrder.fulfilled.type]: (state, action) => {
      state.loading = false;
      state.currentOrder = action.payload; // 直接用payload传参来更新状态
      state.error = null;
    },
    [placeOrder.rejected.type]: (state, action: PayloadAction<string | null>) => {
      state.loading = false;
      state.error = action.payload;
    },
    //  ! 调用购物车的action
    [checkout.pending.type]: (state) => {
      state.loading = true;
    },
    [checkout.fulfilled.type]: (state, action) => {
      state.loading = false;
      state.currentOrder = action.payload; // 购物车列表值为空数组
      state.error = null;
    },
    [checkout.rejected.type]: (state, action: PayloadAction<string | null>) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});
