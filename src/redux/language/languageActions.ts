// 代表不同处理方法的常量
export const CHANGE_LANGUAGE = "change_language";
export const ADD_LANGUAGE = "add_language";

/** 定义接口 */
interface ChangeLanguageAction {
  type: typeof CHANGE_LANGUAGE;
  payload: "zh" | "en";
}

interface AddLanguageAction {
  type: typeof ADD_LANGUAGE;
  payload: { name: string; code: string };
}

//! 导出混合类型
export type LanguageActionTYpes = ChangeLanguageAction | AddLanguageAction;

// 创建action工厂函数
export const changeLanguageActionCreator = (
  language: "zh" | "en"
): ChangeLanguageAction => ({
  type: CHANGE_LANGUAGE,
  payload: language,
});

export const addLanguageActionCreator = (
  name: string,
  code: string
): AddLanguageAction => ({
  type: ADD_LANGUAGE,
  payload: { name, code },
});
