// 2.引入actions
import {
  RecommendProductsAction,
  FETCH_RECOMMEND_PRODUCTS_START,
  FETCH_RECOMMEND_PRODUCTS_FAIL,
  FETCH_RECOMMEND_PRODUCTS_SUCCESS,
} from "./recommendProductsActions";

// 1 定义state
interface RecommendProductsState {
  productList: any[];
  loading: boolean; // 请求状态
  error: string | null; // 错误信息
}
const defaultState: RecommendProductsState = {
  productList: [],
  loading: true,
  error: null,
};

export default (state = defaultState, action: RecommendProductsAction) => {
  // 通过swtich语句 去判断不同的action 进行不同的reducer
  switch (action.type) {
    case FETCH_RECOMMEND_PRODUCTS_START:
      return { ...state, loading: true };
    case FETCH_RECOMMEND_PRODUCTS_SUCCESS:
      return { ...state, loading: false, productList: action.payload };
    case FETCH_RECOMMEND_PRODUCTS_FAIL:
      return { ...state, loading: false, error: action.payload };

    default:
      return state;
  }
  // return state;
};
