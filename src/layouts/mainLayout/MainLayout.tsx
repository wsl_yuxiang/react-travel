import React from "react";
import { Footer, Header } from "@/components";
import styles from "./MainLayout.module.css";

// ! 在react 18之后 react的children 要定义之后才能使用
interface MainLayoutProps {
  children?: React.ReactNode;
}

export const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  return (
    <div>
      <Header />
      <div className={styles.page_content}>{children}</div>
      <Footer />
    </div>
  );
};
