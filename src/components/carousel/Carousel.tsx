import styles from "./Carousel.module.css";
import { Carousel as AntdCarousel, Image } from "antd";
import carousel1 from "@/assets/images/carousel_1.jpg";
import carousel2 from "@/assets/images/carousel_2.jpg";
import carousel3 from "@/assets/images/carousel_3.jpg";

const img = [carousel1, carousel2, carousel3];

export const Carousel: React.FC = () => {
  return (
    <AntdCarousel autoplay className={styles.carousel}>
      {img.map((item, index) => (
        <Image key={index} src={item} />
      ))}
    </AntdCarousel>
  );
};
