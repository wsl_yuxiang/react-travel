import React from "react";
import { Col, Divider, Row } from "antd";
import styles from "./Partner.module.css";
import img1 from "@/assets/images/microsoft-80658_640.png";
import img2 from "@/assets/images/icon-720944_640.png";
import img3 from "@/assets/images/follow-826033_640.png";
import img4 from "@/assets/images/facebook-807588_640.png";

const imgs = [img1, img2, img3, img4];

export const Partner: React.FC = () => {
  return (
    <div>
      <Divider orientation="left">合作伙伴</Divider>
      <Row>
        {imgs.map((e, i) => (
          <Col span={6} key={i}>
            <img src={e} alt="" className={styles.img} />
          </Col>
        ))}
      </Row>
    </div>
  );
};
