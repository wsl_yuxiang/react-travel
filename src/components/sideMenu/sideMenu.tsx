import styles from "./sideMenu.module.css";
import { sideMenuList } from "./mockup";
import { Menu } from "antd";
import { GifOutlined } from "@ant-design/icons";

// 三层嵌套菜单
export const SideMenu: React.FC = () => {
  return (
    <Menu mode="vertical" className={styles["sub-menu"]}>
      {sideMenuList.map((item, index) => (
        <Menu.SubMenu
          key={`side-menu-${index}`}
          title={
            <span>
              <GifOutlined style={{ marginRight: 10 }} />
              {item.title}
            </span>
          }
        >
          {item.subMenu.map((subItem, subIndex) => (
            <Menu.SubMenu
              key={`side-menu-${index}-${subIndex}`}
              title={subItem.title}
            >
              {subItem.subMenu.map((subSubItem, subSubIndex) => (
                <Menu.Item
                  key={`side-menu-${index}-${subIndex}-${subSubIndex}`}
                >
                  {subSubItem}
                </Menu.Item>
              ))}
            </Menu.SubMenu>
          ))}
        </Menu.SubMenu>
      ))}
    </Menu>
  );
};
