import React from "react";
import { Image, Typography } from "antd";
import { useNavigate } from "react-router-dom";

interface productImageProps {
  id: number | string;
  size: "large" | "small"; // ts定义只有这两种取值
  title: string;
  imageSrc: string;
  price: string;
}

export const ProductImage: React.FC<productImageProps> = ({ id, size, title, imageSrc, price }) => {
  const navigate = useNavigate();

  const handleClick = (id: number | string) => {
    console.log("点击了", id);
    navigate(`/detail/${id}`)
  };

  return (
    <>
      {size === "large" ? (
        <Image
          src={imageSrc}
          height={285}
          width={490}
          preview={false}
          onClick={() => {
            handleClick(id);
          }}
        />
      ) : (
        <Image
          src={imageSrc}
          height={120}
          width={240}
          preview={false}
          onClick={() => {
            handleClick(id);
          }}
        />
      )}
      <div>
        <Typography.Text type="secondary">{title.slice(0, 25)}</Typography.Text>
        <Typography.Text type="danger" strong>
          ¥ {price} 起
        </Typography.Text>
      </div>
    </>
  );
};
