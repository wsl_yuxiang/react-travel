import React from "react";
import { Col, Divider, Row } from "antd";
import { ProductImage } from "./ProductImage";
import styles from "./ProductCollection.module.css";

interface IProps {
  title: React.ReactElement;
  sideImage: string;
  products: any[];
}

export const ProductCollection: React.FC<IProps> = ({
  title,
  sideImage,
  products,
}) => {
  return (
    <div className={styles.content}>
      <Divider orientation="left">{title}</Divider>
      <Row>
        <Col span={4}>
          <img src={sideImage} alt="" className={styles["side-image"]} />
        </Col>
        <Col span={20}>
          <Row>
            <Col span={12}>
              <ProductImage
                id={products[0].id}
                size="large"
                title={products[0].title}
                imageSrc={products[0].touristRoutePictures[0].url}
                price={products[0].price}
              />
            </Col>
            <Col span={12}>
              <Row>
                <Col span={12}>
                  <ProductImage
                    id={products[1].id}
                    size="small"
                    title={products[1].title}
                    imageSrc={products[1].touristRoutePictures[0].url}
                    price={products[1].price}
                  />
                </Col>
                <Col span={12}>
                  <ProductImage
                    id={products[2].id}
                    size="small"
                    title={products[2].title}
                    imageSrc={products[2].touristRoutePictures[0].url}
                    price={products[2].price}
                  />
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <ProductImage
                    id={products[3].id}
                    size="small"
                    title={products[3].title}
                    imageSrc={products[3].touristRoutePictures[0].url}
                    price={products[3].price}
                  />
                </Col>
                <Col span={12}>
                  <ProductImage
                    id={products[4].id}
                    size="small"
                    title={products[4].title}
                    imageSrc={products[4].touristRoutePictures[0].url}
                    price={products[4].price}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            {products.slice(5).map((item) => (
              <Col span={6} key={item.id}>
                <ProductImage
                  id={item.id}
                  size="small"
                  title={item.title}
                  imageSrc={item.touristRoutePictures[0].url}
                  price={item.price}
                />
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </div>
  );
};
