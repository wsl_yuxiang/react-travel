import styles from "./Footer.module.css";
import { Layout, Typography } from "antd";
import { useTranslation } from "react-i18next";
// 进行国际化

export const Footer: React.FC = () => {
  const { t } = useTranslation();
  return (
    <div className={styles.App}>
      <Layout.Footer>
        <Typography.Title level={3} style={{ textAlign: "center" }}>
          {t("footer.detail")}
        </Typography.Title>
      </Layout.Footer>
    </div>
  );
};
