import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
// 由于 antd 组件的默认文案是英文，所以需要修改为中文
// import zhCN from "antd/lib/locale/zh_CN";
import moment from "moment";
import "moment/locale/zh-cn";
import "antd/dist/antd.css";

// 引入i18n国际化模块
import "./i18n/configs";
import axios from "axios";
import { Provider } from "react-redux";
import rootStore from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";

moment.locale("zh-cn");

// 设置请求头
axios.defaults.headers["x-icode"] = "68583912BC5000B8";

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <React.StrictMode>
    <Provider store={rootStore.store}>
      {/* 持久化的一个入口 传入加强版的store */}
      <PersistGate persistor={rootStore.persistor}>
        <App />
      </PersistGate>
    </Provider>
  </React.StrictMode>
);
